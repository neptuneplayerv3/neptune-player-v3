/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WidgetUtils;
import com.neptune.common.DataHolder;
import static com.neptune.common.DataHolder.odpFileSecondaryBackupPath;
import com.neptune.common.OdpEditEnum;
import com.neptune.controller.ParentController;
import com.neptune.player.StreamPlayer;
import com.neptune.scheduler.ScheduledTask;
import com.neptune.schema.Media;
import com.neptune.schema.Odpmedia;
import com.neptune.util.DateUtil;
import com.neptune.util.HelperUtility;
import com.neptune.util.JTextFieldLimit;
import com.neptune.util.MouseClickEventMethods;
import com.neptune.util.NeptuneLogger;
import com.neptune.util.PlayListUtils;
import com.neptune.webservice.odp.ExchangeODPService;
import com.neptune.webservice.odp.ODP;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.nio.file.FileSystems;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author vishnu.saraswathy
 */
public class OnDemandMode extends javax.swing.JFrame {

    /**
     * Creates new form RadioMode
     * 
     */
    public OnDemandMode() {
        initComponents(); 
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width=new Double(screenSize.getWidth()).intValue();
        int height=new Double(screenSize.getHeight()).intValue();
        jPanel9.setSize(width, height);
        jLabel27.setSize(width,height);
        int x=(width/2)-600;
        int y=(height/2)-350;
        jPanel1.setLocation(x,y);    
        DataHolder.odpBuildLabel=jLabel3;
        DataHolder.odpDeleteLabel=jLabel24;
        DataHolder.odpEditLabel=jLabel23;
        DataHolder.odpSaveLabel=jLabel20;
        DataHolder.build=true;
        DataHolder.edit=false;
        DataHolder.save=false;
        DataHolder.delete=false;
        jButton2.setBackground(Color.GREEN);
        int volume=65000/2;
        StreamPlayer.changeVolume(volume);
       // DataHolder.currentStation = new Media.Channels.Station();
       // StreamPlayer.stop();
        ((DefaultTableCellRenderer)jTable1.getDefaultRenderer(Object.class)).setOpaque(false);
        jTable2.setBackground(new Color(0,0,0,0) );
        jScrollPane2.getViewport().setOpaque(false);
        jTable1.setDefaultRenderer(Object.class, new OnDemandPlayListPanelv2());
         jTable1.setBorder(new EtchedBorder(EtchedBorder.RAISED));
         DataHolder.programmeLabel=jLabel22;
         DataHolder.currentTrackLabel=jLabel26;
         DataHolder.playerStatusLabel=jLabel13;
         DataHolder.stationLabel=jLabel21;
         
        try {
            NeptuneLogger.getInstance().getLogger().log(Level.INFO, "On Demand Station Selected");
            DataHolder.odpModeStatus = OdpEditEnum.OFFLINE.toString();
            DataHolder.isOdpPlaylistPlaying = false;
            HelperUtility.cleanOdpPlaylist();
            DataHolder.playList=jTable1;
            DataHolder.ondemandCategories=jTable3;
            DefaultTableCellRenderer centerRendererForOndemandCategories = new DefaultTableCellRenderer();
            centerRendererForOndemandCategories.setHorizontalAlignment(SwingConstants.CENTER);
            /*ondemandCategories.getColumnModel().getColumn(0).setCellRenderer(centerRendererForOndemandCategories);
            ondemandCategories.getColumnModel().getColumn(1).setCellRenderer(centerRendererForOndemandCategories);
            ondemandCategories.getColumnModel().getColumn(2).setCellRenderer(centerRendererForOndemandCategories);
            ondemandCategories.getColumnModel().getColumn(3).setCellRenderer(centerRendererForOndemandCategories);
            ondemandCategories.getColumnModel().getColumn(4).setCellRenderer(centerRendererForOndemandCategories);
            ondemandCategories.getColumnModel().getColumn(5).setCellRenderer(centerRendererForOndemandCategories);*/
            
            HelperUtility.getOnDemandTrackListFromUpdatedMediaUpgraded(false);
            if (DataHolder.onDemandTrackList.size() > 0) {
                DataHolder.onDemandOverride = false;
                DataHolder.currentStation = StreamPlayer.getOndemandStation();
                StreamPlayer.fadeOut();
                if (DataHolder.currentStation != null) {
                    DataHolder.currentProgramme = null;
                    DataHolder.currentProgramme = StreamPlayer.getOndemandStationProgramme(DataHolder.currentStation);
                    if (DataHolder.currentProgramme != null) {
                        DataHolder.ondemandCategoryName = DataHolder.currentProgramme.getName();
                    }
                }else {
                    jLabel13.setText("Unavailable");
                }
                if (DataHolder.currentProgramme != null) {
                    DataHolder.programmeLabel.setText(DataHolder.currentProgramme.getName());
                } else {
                    DataHolder.programmeLabel.setText("No Program available");
                }
                DataHolder.currenttrack = null;
                DataHolder.nextTrack = null;
                DataHolder.playListSelectedTrackPath = "";
        }
        } catch (Exception e) {
            NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "Error while Resizing", e);
        }
        try {
            sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(OnDemandMode.class.getName()).log(Level.SEVERE, null, ex);
        }  
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel10 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jSlider1 = new javax.swing.JSlider();
        jPanel6 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jPanel11 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();

        jPanel10.setBackground(new java.awt.Color(0, 0, 51));
        jPanel10.setForeground(new java.awt.Color(0, 0, 51));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 320, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(100, 100));
        setUndecorated(true);
        setResizable(false);

        jPanel9.setLayout(null);

        jPanel1.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("CALL PARTNER SUPPORT FOR HELP AT 903-287-0770");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(320, 660, 550, 30);

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Dialog", 1, 22)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("User Profile Name");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(30, 10, 200, 23);

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/mini.jpg"))); // NOI18N
        jLabel11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel11MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel11);
        jLabel11.setBounds(1120, 10, 30, 20);

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/close.jpg"))); // NOI18N
        jLabel12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel12MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel12);
        jLabel12.setBounds(1150, 10, 40, 30);

        jPanel3.setBackground(new java.awt.Color(0, 0, 51));

        jLabel13.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Station - On Demand");

        jPanel4.setBackground(new java.awt.Color(153, 204, 255));

        jLabel18.setText("program");

        jLabel22.setText("jLabel22");

        jLabel17.setText("Station");

        jLabel21.setText("On Demand");

        jLabel25.setText("Track");

        jLabel26.setText("Unavailable");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(jLabel17)
                    .addComponent(jLabel25))
                .addGap(54, 54, 54)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21)
                    .addComponent(jLabel22)
                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jLabel22))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(jLabel26))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(153, 204, 255));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 269, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 127, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(53, 53, 53))
        );

        jPanel1.add(jPanel3);
        jPanel3.setBounds(230, 50, 630, 180);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/logo.png"))); // NOI18N
        jPanel1.add(jLabel2);
        jLabel2.setBounds(880, 50, 320, 120);

        jSlider1.setBackground(new java.awt.Color(0, 204, 204));
        jSlider1.setForeground(new java.awt.Color(0, 51, 153));
        jSlider1.setMajorTickSpacing(100);
        jSlider1.setMinimum(1);
        jSlider1.setMinorTickSpacing(10);
        jSlider1.setSnapToTicks(true);
        jSlider1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jSlider1.setOpaque(false);
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });
        jPanel1.add(jSlider1);
        jSlider1.setBounds(890, 160, 300, 23);

        jPanel6.setBackground(new java.awt.Color(0, 0, 51));

        jLabel28.setBackground(new java.awt.Color(153, 204, 255));
        jLabel28.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("Music Genres");
        jLabel28.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel28.setOpaque(true);
        jLabel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel28MouseClicked(evt);
            }
        });
        jLabel28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel28KeyPressed(evt);
            }
        });

        jLabel29.setBackground(new java.awt.Color(153, 204, 255));
        jLabel29.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("On Demand PlayList");
        jLabel29.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel29.setOpaque(true);
        jLabel29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel29MouseClicked(evt);
            }
        });
        jLabel29.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel29KeyPressed(evt);
            }
        });

        jLabel30.setBackground(new java.awt.Color(153, 204, 255));
        jLabel30.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("Hot Keys");
        jLabel30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel30.setOpaque(true);
        jLabel30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel30MouseClicked(evt);
            }
        });
        jLabel30.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel30KeyPressed(evt);
            }
        });

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/stop.png"))); // NOI18N
        jLabel15.setText("jLabel15");
        jLabel15.setToolTipText("Stop/Reset");
        jLabel15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel15MouseClicked(evt);
            }
        });

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/play.png"))); // NOI18N
        jLabel16.setToolTipText("Play");
        jLabel16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
        });

        jPanel7.setOpaque(false);

        jLabel19.setBackground(new java.awt.Color(153, 204, 255));
        jLabel19.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/odp.png"))); // NOI18N
        jLabel19.setToolTipText("On Demand Playlist");
        jLabel19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel19MouseClicked(evt);
            }
        });

        jLabel3.setBackground(new java.awt.Color(153, 204, 255));
        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/build.png"))); // NOI18N
        jLabel3.setToolTipText("Build");
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });

        jLabel20.setBackground(new java.awt.Color(153, 204, 255));
        jLabel20.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save.png"))); // NOI18N
        jLabel20.setToolTipText("Save");
        jLabel20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel20.setEnabled(false);
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });

        jLabel23.setBackground(new java.awt.Color(153, 204, 255));
        jLabel23.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/edit.png"))); // NOI18N
        jLabel23.setToolTipText("Edit");
        jLabel23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel23.setEnabled(false);
        jLabel23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel23MouseClicked(evt);
            }
        });

        jLabel24.setBackground(new java.awt.Color(153, 204, 255));
        jLabel24.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/delete.png"))); // NOI18N
        jLabel24.setToolTipText("Delete");
        jLabel24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel24.setEnabled(false);
        jLabel24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel24MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel24, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jLabel9.setBackground(new java.awt.Color(153, 204, 255));
        jLabel9.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("On Demand Tabs");
        jLabel9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel9.setOpaque(true);
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });
        jLabel9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel9KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 115, Short.MAX_VALUE)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 48, Short.MAX_VALUE))
                .addGap(0, 6, Short.MAX_VALUE))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel6);
        jPanel6.setBounds(0, 250, 1200, 50);

        jScrollPane2.setOpaque(false);

        jTable2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTable2.setOpaque(false);
        jTable2.setRowHeight(35);
        jTable2.setSelectionBackground(new java.awt.Color(204, 255, 255));
        jTable2.setSelectionForeground(new java.awt.Color(0, 0, 0));
        jTable2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable2.setShowHorizontalLines(false);
        jTable2.setShowVerticalLines(false);
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(0, 300, 220, 320);

        jButton1.setBackground(new java.awt.Color(0, 51, 102));
        jButton1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Radio Mode");
        jButton1.setFocusPainted(false);
        jButton1.setSelected(true);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(890, 200, 120, 40);

        jButton2.setBackground(new java.awt.Color(0, 51, 102));
        jButton2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("On Demand Mode");
        jButton2.setFocusPainted(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(1020, 200, 160, 40);

        jPanel2.setOpaque(false);
        jPanel2.setLayout(null);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/connection.png"))); // NOI18N
        jLabel7.setText("Refresh Play List");
        jLabel7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel7);
        jLabel7.setBounds(10, 40, 180, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/wifi.png"))); // NOI18N
        jLabel6.setText("Connection Status");
        jLabel6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel2.add(jLabel6);
        jLabel6.setBounds(10, 0, 180, 30);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/backup.png"))); // NOI18N
        jLabel8.setText("Back up");
        jLabel8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel8MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel8);
        jLabel8.setBounds(10, 120, 180, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/folder.png"))); // NOI18N
        jLabel10.setText("Version 1.1");
        jLabel10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel2.add(jLabel10);
        jLabel10.setBounds(10, 160, 180, 30);

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 255, 255));
        jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/download.png"))); // NOI18N
        jLabel31.setText("Downloading(%)");
        jLabel31.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel31.setEnabled(false);
        jPanel2.add(jLabel31);
        jLabel31.setBounds(10, 80, 180, 30);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(30, 50, 190, 200);

        jTable1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "", "", "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jTable1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTable1.setRowHeight(60);
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                jTable1AncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(240, 300, 960, 320);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jTable3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {}
            },
            new String [] {

            }
        ));
        jTable3.setToolTipText("Click Stop to Reset");
        jTable3.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable3.setCellSelectionEnabled(true);
        jTable3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTable3.setRowHeight(37);
        jTable3.setSelectionBackground(new java.awt.Color(204, 255, 255));
        jTable3.setTableHeader(null);
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTable3);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1200, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel8);
        jPanel8.setBounds(0, 620, 1200, 40);

        jPanel11.setBackground(new java.awt.Color(0, 0, 51));
        jPanel11.setForeground(new java.awt.Color(0, 0, 51));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 320, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel11);
        jPanel11.setBounds(220, 300, 20, 320);

        jLabel1.setBackground(new java.awt.Color(0, 0, 102));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/neptune/pictures/background.jpg"))); // NOI18N
        jLabel1.setText("b ");
        jLabel1.setPreferredSize(new java.awt.Dimension(1200, 700));
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, 0, 1200, 700);

        jPanel9.add(jPanel1);
        jPanel1.setBounds(0, 10, 1200, 700);

        jLabel27.setBackground(new java.awt.Color(0, 0, 51));
        jLabel27.setOpaque(true);
        jPanel9.add(jLabel27);
        jLabel27.setBounds(0, 0, 1200, 710);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 1200, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseClicked
        this.setState(Frame.ICONIFIED);   // TODO add your handling code here:
    }//GEN-LAST:event_jLabel11MouseClicked

    private void jLabel12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseClicked
        System.exit(0);    // TODO add your handling code here:
    }//GEN-LAST:event_jLabel12MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
          NeptuneLogger.getInstance().getLogger().log(Level.INFO, "Other Station...");
        DataHolder.odpModeStatus = OdpEditEnum.BUILD.toString();
        DataHolder.isOdpEditLogoRequired = false;
        DataHolder.currentStation = StreamPlayer.getNotOndemandStation();
        DataHolder.currenttrack = null;
        DataHolder.nextTrack = null;
        DataHolder.currentTrackPath = null;
        DataHolder.currentTrackLabel.setText("Unavailable");
        DataHolder.nextTrackLabel.setText("Unavailable");
        StreamPlayer.fadeOut();
        dispose();
        new ParentController().play();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jLabel9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel9KeyPressed
       jLabel9.setBackground(Color.GREEN);  
       jLabel28.setBackground(new Color(153,204,255)); 
       jLabel29.setBackground(new Color(153,204,255)); 
       jLabel30.setBackground(new Color(153,204,255)); 
    }//GEN-LAST:event_jLabel9KeyPressed

    private void jLabel28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel28KeyPressed
      // TODO add your handling code here:
    }//GEN-LAST:event_jLabel28KeyPressed

    private void jLabel29KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel29KeyPressed
      // TODO add your handling code here:
    }//GEN-LAST:event_jLabel29KeyPressed

    private void jLabel30KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel30KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel30KeyPressed

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
       jLabel9.setBackground(Color.GREEN);  
       jLabel28.setBackground(new Color(153,204,255)); 
       jLabel29.setBackground(new Color(153,204,255)); 
       jLabel30.setBackground(new Color(153,204,255)); // TODO add your handling code here:
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jLabel28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel28MouseClicked
          jLabel9.setBackground(new Color(153,204,255));  
       jLabel28.setBackground(Color.GREEN); 
       jLabel29.setBackground(new Color(153,204,255)); 
       jLabel30.setBackground(new Color(153,204,255));  // TODO add your handling code here:
    }//GEN-LAST:event_jLabel28MouseClicked

    private void jLabel29MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel29MouseClicked
        jLabel9.setBackground(new Color(153,204,255));  
       jLabel28.setBackground(new Color(153,204,255)); 
       jLabel29.setBackground(Color.GREEN); 
       jLabel30.setBackground(new Color(153,204,255));  // TODO add your handling code here:
    }//GEN-LAST:event_jLabel29MouseClicked

    private void jLabel30MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MouseClicked
        jLabel9.setBackground(new Color(153,204,255));  
       jLabel28.setBackground(new Color(153,204,255)); 
       jLabel29.setBackground(new Color(153,204,255)); 
       jLabel30.setBackground(Color.GREEN);  // TODO add your handling code here:
    }//GEN-LAST:event_jLabel30MouseClicked

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        switch (OdpEditEnum.valueOf(DataHolder.odpModeStatus.toUpperCase())) {
//==============================================================================================================================================
                    case LIST:
                        DataHolder.odpPlayList=jTable2;
                        PlayListUtils.playListMouseClickedWhenOnDemandOverrideIsOnInOdpListMode(evt);
                        break;
//==============================================================================================================================================                        
                    case EDIT:
                        PlayListUtils.playListMouseClickedInOdpEditModeStatus(evt);
                        break;
//==============================================================================================================================================                                                
                    case BUILD:
                        if (null == DataHolder.odpMedia) {
                            HelperUtility.initializeODPMedia();
                        }
                        DataHolder.odpPlayList=jTable2;
                        DataHolder.odpPlaylistModel = (javax.swing.table.DefaultTableModel) DataHolder.odpPlayList.getModel();
                        OnDemandPlayListPanelv2 panel = (OnDemandPlayListPanelv2) DataHolder.playList.getValueAt(
                                DataHolder.playList.getSelectedRow(), DataHolder.playList.getSelectedColumn());
                        if (null != panel) {
//                            switch (HelperUtility.covertBooleanToInt(HelperUtility.addOdpMaxtime(panel.getPlayTimeLabel().getText()))) {
//                                case 1:
                            com.neptune.schema.Odpmedia.Playlist.Track track = new com.neptune.schema.Odpmedia.Playlist.Track();
                            track.setName(panel.getSongTitleLabel().getName());
                            track.setLabel(panel.getSongTitleLabel().getText());
                            track.setId(Long.parseLong(String.valueOf(DataHolder.odpLastRow)));
                            track.setTime(panel.getPlayTimeLabel().getText());
                            if (null == DataHolder.odpTrackList) {
                                DataHolder.odpTrackList = new LinkedList<Odpmedia.Playlist.Track>();
                            }
                            if (!DataHolder.odpTrackList.contains(track)) {
                                if (DataHolder.odpLastRow == DataHolder.odpPlayList.getRowCount()) {
                                    DataHolder.odpPlaylistModel.addRow(new Object[]{});
                                }
                                DataHolder.odpTrackList.add(track);
                                HelperUtility.addOdpMaxtime(panel.getPlayTimeLabel().getText());
                                DataHolder.odpPlaylistModel.setValueAt(panel.getSongTitleLabel().getText(), DataHolder.odpLastRow, 0);
                                DataHolder.odpLastRow++;
                            }
//                                    break;
//                                case 0:
//                                    JOptionPane.showMessageDialog(null, "Your On Demand Playlist is at maximum length. Please remove or replace content to continue.", "", JOptionPane.WARNING_MESSAGE);
//                                    break;
//                            }
                        }
                        break;
//==============================================================================================================================================                        
                    case OFFLINE:
                        PlayListUtils.playListMouseClicked(evt);
                        if (evt.getClickCount() == 2) {
                            if (DataHolder.playListSelectedTrackPath != null && !DataHolder.playListSelectedTrackPath.isEmpty()) {
                                try {
                                    if (DataHolder.playListSelectedTrackPath.equalsIgnoreCase(DataHolder.currentTrackPath) && DataHolder.isPlaying) {
                                        return;
                                    }
                                    StreamPlayer.fadeOut();
                                    if (DataHolder.playListSelectedTrackPath != null) {
                                        if (!DataHolder.playListSelectedTrackPath.isEmpty()) {
                                            DataHolder.currentTrackPath = DataHolder.playListSelectedTrackPath;
                                            DataHolder.programmeLabel.setText(DataHolder.ondemandCategoryName);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    DataHolder.onDemandOverride = true;
                                }
                            }
                        }
                        break;
                } 
    }//GEN-LAST:event_jTable1MouseClicked

    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged
     int value=jSlider1.getValue();
    //System.out.println(value);
    StreamPlayer.changeVolume(value);
    }//GEN-LAST:event_jSlider1StateChanged

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
 switch (OdpEditEnum.valueOf(DataHolder.odpModeStatus.toUpperCase())) {
            case EDIT:
            case BUILD:
                
                if(!"".equalsIgnoreCase((String) DataHolder.odpPlayList.getValueAt(DataHolder.odpPlayList.getSelectedRow(), DataHolder.odpPlayList.getSelectedColumn()))){
                    String[] choices = {"Remove Song", "Replace Song", "Cancel"};
                while (true) {
                    int response = JOptionPane.showOptionDialog(
                            null // Center in window.
                            , "What do you want to do?" // Message
                            , "" // Title in titlebar
                            , JOptionPane.YES_NO_OPTION // Option type
                            , JOptionPane.PLAIN_MESSAGE // messageType
                            , null // Icon (none)
                            , choices // Button text as above.
                            , "DEFAULT" // Default button's label
                    );

                    //... Use a switch statement to check which button was clicked.
                    switch (response) {
//=====================================R E M O V E=================================================================================                        
                        case 0:
                            for (Odpmedia.Playlist playlist : DataHolder.odpMedia.playlist) {
                                if (playlist.getId().equalsIgnoreCase(DataHolder.currentOdpPlaylist)) {
//                                    List<Odpmedia.Playlist.Track> listOfPlayListTrack = new ArrayList(playlist.getTrack());
//                                    Collections.sort(listOfPlayListTrack, new OdpTrackComparator());
                                    Iterator iter = playlist.getTrack().iterator();
                                    if (null == DataHolder.odpTrackList) {
                                        DataHolder.odpTrackList = new LinkedList<Odpmedia.Playlist.Track>();
                                    } else {
                                        DataHolder.odpTrackList.clear();
                                    }
                                    while (iter.hasNext()) {
                                        Odpmedia.Playlist.Track track = (Odpmedia.Playlist.Track) iter.next();
                                        if (track.getLabel().equalsIgnoreCase((String) DataHolder.odpPlayList.getValueAt(DataHolder.odpPlayList.getSelectedRow(), DataHolder.odpPlayList.getSelectedColumn()))) {
                                            iter.remove();
                                            DataHolder.isOdpEditLogoRequired = false;
                                            HelperUtility.cleanOdpPlaylist();
                                            DataHolder.isOdpEditLogoRequired = true;
                                            for (Odpmedia.Playlist.Track track1 : playlist.getTrack()) {
                                                if (DataHolder.odpLastRow == DataHolder.odpPlayList.getRowCount()) {
                                                    DataHolder.odpPlaylistModel.addRow(new Object[]{});
                                                }
                                                HelperUtility.addOdpMaxtime(track1.getTime());
                                                DataHolder.odpPlaylistModel.setValueAt(track1.getLabel(), DataHolder.odpLastRow, 0);
                                                DataHolder.odpLastRow++;
                                            }
                                            break;
                                        }
                                    }
                                    DataHolder.odpTrackList.addAll(playlist.getTrack());
                                    System.out.println("Odp Track size : " + DataHolder.odpTrackList.size());

                                }
                            }
                            break;
//=====================================R E P L A C E=================================================================================                                
                        case 1:
                            DataHolder.odpEditEnumStatus = OdpEditEnum.REPLACE.getOdpEditEnum();
                            HelperUtility.getOnDemandTrackListFromUpdatedMediaUpgraded(false);
                            break;
                        case 2:

                            break;
                        case 3:
                        case -1:
                            System.exit(0);
                        default:
                            JOptionPane.showMessageDialog(null, "Unexpected response " + response);
                    }
                    break;
                }
                }
        }        
    }//GEN-LAST:event_jTable2MouseClicked

    private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
 try {
                    switch (OdpEditEnum.valueOf(DataHolder.odpModeStatus.toUpperCase())) {
                        case LIST:
                            PlayListUtils.remapOdpPlaylist();
                            StreamPlayer.fadeOut();
                            DataHolder.isOdpPlaylistPlaying = true;
                            DataHolder.currentTrackPath = null;
                            DataHolder.onDemandOverride = true;
                            DataHolder.odpCurrentTrackId = 0L;
                            break;
                        case OFFLINE:
                            if (DataHolder.playListSelectedTrackPath.equalsIgnoreCase(DataHolder.currentTrackPath) && DataHolder.isPlaying) {
                                return;
                            }
                            break;
                    }
                    StreamPlayer.fadeOut();
                    if (DataHolder.playListSelectedTrackPath != null) {
                    if (!DataHolder.playListSelectedTrackPath.isEmpty()) {
                        DataHolder.currentTrackPath = DataHolder.playListSelectedTrackPath;
                        DataHolder.programmeLabel.setText(DataHolder.ondemandCategoryName);
                    }
                }
                DataHolder.onDemandOverride = true;  
                } catch (Exception e) {
                    e.printStackTrace();
                }
             // TODO add your handling code here:
    }//GEN-LAST:event_jLabel16MouseClicked

    private void jLabel15MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel15MouseClicked
                StreamPlayer.fadeOut();
                jLabel19.setForeground(Color.white);
                
                DefaultTableModel model=(DefaultTableModel)jTable2.getModel();
                DefaultTableModel playListDataModel=(DefaultTableModel) DataHolder.playList.getModel();
                for (int row = 0; row < playListDataModel.getRowCount(); row++) {
                for (int column = 0; column < playListDataModel.getColumnCount(); column++) {
                if (playListDataModel.getValueAt(row, column) != null) {
                   // OnDemandPlayListPanelv2 panel = (OnDemandPlayListPanelv2) playListDataModel.getValueAt(row, column);
                    playListDataModel.setValueAt(null, row, column);
                   }
                  }
                 }
                DataHolder.playingODP=false;
                DataHolder.ondemandCategories.setEnabled(true);
                 DataHolder.isOdpPlaylistPlaying = false;
                if (null != DataHolder.ondemandCategories.getValueAt(0,0)) {
                    DataHolder.onDemandOverride = true;
                    DataHolder.currenttrack = null;
                    DataHolder.nextTrack = null;
                    DataHolder.currentTrackPath = null;
                    DataHolder.nextTrackPath = null;
                    DataHolder.playListSelectedTrackPath = null;
                    DataHolder.onDemandTrackListDisplayed.clear();
                    DataHolder.ondemandCategoryName = DataHolder.ondemandCategories.getValueAt(0,0).toString();
                    HelperUtility.getOnDemandTrackListFromUpdatedMediaUpgraded(true);
                }
                model.setRowCount(0);
                DataHolder.odpModeStatus="offline";
                DataHolder.isPlaying = false;
                DataHolder.playerStatusLabel.setText("Player stopped");
                DataHolder.onDemandOverride = false;
                DataHolder.currentTrackLabel.setText("Stopped");
                DataHolder.nextTrackLabel.setText("Stopped");
                DataHolder.currenttrack = null;
                DataHolder.nextTrack = null;
                DataHolder.playListSelectedTrackPath = "";
                StreamPlayer.fadeOut();
                switch (OdpEditEnum.valueOf(DataHolder.odpModeStatus.toUpperCase())) {
                    case BUILD:
                    case EDIT:
                    case LIST:
                    default:
                        jLabel23.setEnabled(false);
                        jLabel24.setEnabled(false);
                        jLabel3.setEnabled(true);
                        jLabel20.setEnabled(false);
                        DataHolder.edit=false;
                        DataHolder.delete=false;
                        DataHolder.save=false;
                        DataHolder.build=true;
                       // jLabel15MouseClicked(null);
                        break;
                }   
                
    }//GEN-LAST:event_jLabel15MouseClicked

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        if(!DataHolder.playingODP){
        switch (OdpEditEnum.valueOf(DataHolder.odpModeStatus.toUpperCase())) {
            case EDIT:
            case BUILD:
            default:
                if (DataHolder.isOdpPlaylistPlaying) {
                    StreamPlayer.stop();
                }
                DataHolder.isOdpPlaylistPlaying = false;
                if (null != DataHolder.ondemandCategories.getValueAt(DataHolder.ondemandCategories.getSelectedRow(),
                        DataHolder.ondemandCategories.getSelectedColumn())) {
                    DataHolder.onDemandOverride = true;
                    DataHolder.currenttrack = null;
                    DataHolder.nextTrack = null;
                    DataHolder.currentTrackPath = null;
                    DataHolder.nextTrackPath = null;
                    DataHolder.playListSelectedTrackPath = null;
                    DataHolder.onDemandTrackListDisplayed.clear();
                    DataHolder.ondemandCategoryName = DataHolder.ondemandCategories.getValueAt(
                            DataHolder.ondemandCategories.getSelectedRow(), DataHolder.ondemandCategories.getSelectedColumn()).toString();
                    HelperUtility.getOnDemandTrackListFromUpdatedMediaUpgraded(true);
                }
                break;
        }
        }// TODO add your handling code here:
    }//GEN-LAST:event_jTable3MouseClicked

    private void jLabel19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseClicked
        jLabel19.setForeground(Color.GREEN); 
        DataHolder.odpPlayListTable=jTable1;
        jLabel23.setEnabled(true);
        jLabel24.setEnabled(true);
        jLabel3.setEnabled(false);
        jLabel20.setEnabled(false);
        DataHolder.save=false;
        DataHolder.build=false;
        DataHolder.edit=true;
        DataHolder.delete=true;
        DefaultTableModel playListDataModel=(DefaultTableModel) DataHolder.playList.getModel();
        for (int row = 0; row < playListDataModel.getRowCount(); row++) {
         for (int column = 0; column < playListDataModel.getColumnCount(); column++) {
                if (playListDataModel.getValueAt(row, column) != null) {
                   // OnDemandPlayListPanelv2 panel = (OnDemandPlayListPanelv2) playListDataModel.getValueAt(row, column);
                    playListDataModel.setValueAt(null, row, column);
                }
            }
        }
 MouseClickEventMethods.odpPlayListButtonMouseClickeEvent(evt); // TODO add your handling code here:
    }//GEN-LAST:event_jLabel19MouseClicked

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
      if(DataHolder.build) {
        DefaultTableModel model=(DefaultTableModel) jTable2.getModel();
        model.setRowCount(0);
        try {
            DataHolder.odpModeStatus = OdpEditEnum.BUILD.toString();
            jLabel3.setEnabled(false);
            jLabel23.setEnabled(false);
            jLabel24.setEnabled(false);
            jLabel20.setEnabled(true);
            DataHolder.save=true;
            DataHolder.build=false;
            DataHolder.edit=false;
            DataHolder.delete=false;
            if (null == DataHolder.odpTrackList) {
                DataHolder.odpTrackList = new LinkedList<Odpmedia.Playlist.Track>();
            } else {
                DataHolder.odpTrackList.clear();
                NeptuneLogger.getInstance().getLogger().log(Level.INFO, "Tracklist cleared...");
            }
            HelperUtility.cleanOdpPlaylist();
            jLabel19.setEnabled(false);
            if (!DataHolder.odpFile.exists()) {
                FileUtils.copyFile(DataHolder.odpPrimaryBackupFile, DataHolder.odpFile);
            }
            if (DataHolder.odpFile.exists()) {
                NeptuneLogger.getInstance().getLogger().log(Level.INFO, "Parsing existing file....");
                try {
                    JAXBContext jaxbContext = JAXBContext.newInstance(Odpmedia.class);
                    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                    DataHolder.odpMedia = (Odpmedia) jaxbUnmarshaller.unmarshal(DataHolder.odpFile);
                } catch (JAXBException ex) {
                    NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, null, ex);
                }
            } else {
                NeptuneLogger.getInstance().getLogger().log(Level.INFO, "Creating new odp");
                HelperUtility.initializeODPMedia();
            }
        } catch (IOException ex) {
            Logger.getLogger(OdpHeader.class.getName()).log(Level.SEVERE, null, ex);
        }       
      }// TODO add your handling code here:
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel24MouseClicked
    if(DataHolder.delete) {
         try {
         OnDemandPlayListPanelv2 panel = (OnDemandPlayListPanelv2) DataHolder.playList.getValueAt(
                        DataHolder.playList.getSelectedRow(), DataHolder.playList.getSelectedColumn());
                if (null != panel) {
                    Iterator iter = DataHolder.odpMedia.playlist.iterator();
                    Odpmedia.Playlist playlist = null;
                    while (iter.hasNext()) {
                        playlist = (Odpmedia.Playlist) iter.next();
                        if (panel.getSongTitleLabel().getText().equalsIgnoreCase(playlist.getId())) {
                            iter.remove();
                            JAXBContext jaxbContext = JAXBContext.newInstance(Odpmedia.class);
                            Marshaller jaxbmarshaller = jaxbContext.createMarshaller();
                            jaxbmarshaller.marshal(DataHolder.odpMedia, new File(DataHolder.odpFilePath));
                            HelperUtility.cleanOdpPlaylist();
//                            List all the saved ODP
                            MouseClickEventMethods.odpPlayListButtonMouseClickeEvent(null);
                            playlist = null;
                            new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        if (DataHolder.odpFilePath != null) {
                                            if (DataHolder.odpFile.exists()) {
                                                FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpPrimaryBackupFile);
                                                DataHolder.odpSecondaryBackupFile = new File(odpFileSecondaryBackupPath + FileSystems.getDefault().getSeparator()
                                                        + DateUtil.getInstance().formatDate(new Date(), DateUtil.DATE_FORMAT_MMDDYYYY) + ".bk");
                                                if (!DataHolder.odpSecondaryBackupFile.getParentFile().exists()) {
                                                    DataHolder.odpSecondaryBackupFile.getParentFile().mkdirs();
                                                }
                                                if (!DataHolder.odpSecondaryBackupFile.exists()) {
                                                    DataHolder.odpSecondaryBackupFile.createNewFile();
                                                }
                                                FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpSecondaryBackupFile);
                                                ExchangeODPService exchangeODPService = new ExchangeODPService();
                                                ODP odp = new ODP();
                                                odp.setPartnerName(DataHolder.configProperties.getProperty("userName"));
                                                odp.setMacID(DataHolder.macAddress);
                                                odp.setToken(DataHolder.configProperties.getProperty("DeviceToken"));
                                                BufferedReader bufferedReader = new BufferedReader(new FileReader(DataHolder.odpFile));
                                                StringBuilder stringBuilder = new StringBuilder();
                                                String line = null;
                                                while ((line = bufferedReader.readLine()) != null) {
                                                    stringBuilder.append(line).append("\n");
                                                }
                                                odp.setBackUpText(stringBuilder.toString());
                                                exchangeODPService.getExchangeODPServiceSoap().setODPBackUp(odp);
                                                NeptuneLogger.getInstance().getLogger().log(Level.INFO, "ODP BackUp ----> SUCCESSFULL");
                                            }
                                        }
                                    } catch (Exception e) {
                                        NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "ODP BackUp ----> UNSUCCESSFULL", e);
                                    }
                                }
                            }).start();
                            break;
                        }
                    }
                }
         }catch (JAXBException ex) {
                    Logger.getLogger(OdpHeader.class.getName()).log(Level.SEVERE, null, ex);
                }
}// TODO add your handling code here:
    }//GEN-LAST:event_jLabel24MouseClicked

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
   if(DataHolder.save){
        try {
            StreamPlayer.fadeOut();
            JAXBContext jaxbContext = JAXBContext.newInstance(Odpmedia.class);
            Marshaller jaxbmarshaller = jaxbContext.createMarshaller();
            for (Odpmedia.Playlist playlist1 : DataHolder.odpMedia.getPlaylist()) {
                Long i = 0L;
                for (Odpmedia.Playlist.Track track : playlist1.getTrack()) {
                    track.setId(i);
                    i++;
                }
            }
            switch (OdpEditEnum.valueOf(DataHolder.odpModeStatus.toUpperCase())) {
                case BUILD:
                    boolean odpSaved = false;
                    while (!odpSaved) {
                        odpSaved = true;
                        jLabel3.setEnabled(true);
                        jLabel19.setEnabled(true);
                        JTextField odpPlaylistName = new JTextField();
                        odpPlaylistName.setColumns(30);
                        odpPlaylistName.setSize(new Dimension(150, 60));
                        odpPlaylistName.setDocument(new JTextFieldLimit(30));
                        odpPlaylistName.setText("");

                        JButton cancel = new JButton("Cancel");
                        final JComponent[] inputs = new JComponent[]{
                            new JLabel("Please provide ODP name."),
                            odpPlaylistName,};
                        JOptionPane.showMessageDialog(null, inputs, "ODP Playlist", JOptionPane.ERROR_MESSAGE);
                        if (odpPlaylistName.getText().isEmpty()) {
                            odpPlaylistName.setText("DefaultODP");
                        }
//                        check if there is already a playlist with the same name
                        for (Odpmedia.Playlist playlist : DataHolder.odpMedia.getPlaylist()) {
                            if (playlist.getId().equalsIgnoreCase(odpPlaylistName.getText())) {
                                JOptionPane.showMessageDialog(null, playlist.getId() + " already Exists. Please choose a different name.", "ODP Playlist with same name exists", JOptionPane.ERROR_MESSAGE);
                                odpSaved = false;//sets unsaved msg after displaying a dialogue
                                break;
                            }
                        }
//                        save the odp if there is no playlist with same name already in the odp media
                        if (odpSaved) {
                            Odpmedia.Playlist playlist = new Odpmedia.Playlist();
                            playlist.setId(odpPlaylistName.getText());
                            playlist.setTrack(DataHolder.odpTrackList);
                            playlist.setDate(DateUtil.getInstance().formatDate(new Date(), DateUtil.DATE_FORMAT_YYMMDD));
                            DataHolder.odpMedia.getPlaylist().add(playlist);
                            if (null != DataHolder.odpMedia && DataHolder.odpMedia.getPlaylist() != null && DataHolder.odpMedia.getPlaylist().size() > 0) {
                                jaxbmarshaller.marshal(DataHolder.odpMedia, DataHolder.odpFile);
                                FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpPrimaryBackupFile);
                                DataHolder.odpSecondaryBackupFile = new File(odpFileSecondaryBackupPath + FileSystems.getDefault().getSeparator()
                                        + DateUtil.getInstance().formatDate(new Date(), DateUtil.DATE_FORMAT_MMDDYYYY) + ".bk");
                                if (!DataHolder.odpSecondaryBackupFile.getParentFile().exists()) {
                                    DataHolder.odpSecondaryBackupFile.getParentFile().mkdirs();
                                }
                                if (!DataHolder.odpSecondaryBackupFile.exists()) {
                                    DataHolder.odpSecondaryBackupFile.createNewFile();
                                }
                                FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpSecondaryBackupFile);
                            }
                           jLabel19MouseClicked(null);
                        }

                    }
                    HelperUtility.cleanOdpPlaylist();
                    break;
                case EDIT:
                    try {
                        if (null != DataHolder.odpMedia && DataHolder.odpMedia.getPlaylist() != null && DataHolder.odpMedia.getPlaylist().size() > 0) {
                            jaxbmarshaller.marshal(DataHolder.odpMedia, DataHolder.odpFile);
                            FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpPrimaryBackupFile);
                            DataHolder.odpSecondaryBackupFile = new File(odpFileSecondaryBackupPath + FileSystems.getDefault().getSeparator()
                                    + DateUtil.getInstance().formatDate(new Date(), DateUtil.DATE_FORMAT_MMDDYYYY) + ".bk");
                            if (!DataHolder.odpSecondaryBackupFile.getParentFile().exists()) {
                                DataHolder.odpSecondaryBackupFile.getParentFile().mkdirs();
                            }
                            if (!DataHolder.odpSecondaryBackupFile.exists()) {
                                DataHolder.odpSecondaryBackupFile.createNewFile();
                            }
                            FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpSecondaryBackupFile);
                        }
                    } catch (Exception e) {
                        NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, null, e);
                    } finally {
                        HelperUtility.cleanOdpPlaylist();
                        PlayListUtils.playListMouseClickedWhenOnDemandOverrideIsOnInOdpListMode(null);
                    }
                    break;
            }
        } catch (JAXBException ex) {
            NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OdpHeader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DataHolder.odpModeStatus = OdpEditEnum.OFFLINE.toString();
            DataHolder.odpEditEnumStatus = OdpEditEnum.OFFLINE.toString();
            DataHolder.isOdpEditLogoRequired = false;
            DataHolder.odpPlaylistButton.setEnabled(true);
            if (null == DataHolder.odpPlayList) {
                DataHolder.odpTrackList = new LinkedList<Odpmedia.Playlist.Track>();
            } else {
                DataHolder.odpTrackList.clear();
                NeptuneLogger.getInstance().getLogger().log(Level.INFO, "Tracklist cleared...");
            }
            DataHolder.currentOdpPlaylist = "";
            new Thread(new Runnable() {
                public void run() {
                    try {
                        if (DataHolder.odpFilePath != null) {
                            if (!DataHolder.odpFile.exists()) {
                                FileUtils.copyFile(DataHolder.odpPrimaryBackupFile, DataHolder.odpFile);
                                DataHolder.odpSecondaryBackupFile = new File(odpFileSecondaryBackupPath + FileSystems.getDefault().getSeparator()
                                        + DateUtil.getInstance().formatDate(new Date(), DateUtil.DATE_FORMAT_MMDDYYYY) + ".bk");
                                if (!DataHolder.odpSecondaryBackupFile.getParentFile().exists()) {
                                    DataHolder.odpSecondaryBackupFile.getParentFile().mkdirs();
                                }
                                if (!DataHolder.odpSecondaryBackupFile.exists()) {
                                    DataHolder.odpSecondaryBackupFile.createNewFile();
                                }
                                FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpSecondaryBackupFile);
                            }

                            if (DataHolder.odpFile.exists()) {
                                ExchangeODPService exchangeODPService = new ExchangeODPService();
                                ODP odp = new ODP();
                                odp.setPartnerName(DataHolder.configProperties.getProperty("userName"));
                                odp.setMacID(DataHolder.macAddress);
                                odp.setToken(DataHolder.configProperties.getProperty("DeviceToken"));
                                BufferedReader bufferedReader = new BufferedReader(new FileReader(DataHolder.odpFile));
                                StringBuilder stringBuilder = new StringBuilder();
                                String line = null;
                                while ((line = bufferedReader.readLine()) != null) {
                                    stringBuilder.append(line).append("\n");
                                }
                                odp.setBackUpText(stringBuilder.toString());
                                exchangeODPService.getExchangeODPServiceSoap().setODPBackUp(odp);
                                NeptuneLogger.getInstance().getLogger().log(Level.INFO, "SAVE --> ODPBackUp -----> SUCCESSFULL");
                            }
                        }
                    } catch (Exception e) {
                        NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "SAVE --> ODPBackUp -----> UNSUCCESSFULL", e);
                    }
                }
            }).start();
        } 
        jLabel19MouseClicked(evt);
   }
// TODO add your handling code here:
    }//GEN-LAST:event_jLabel20MouseClicked

    private void jLabel23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel23MouseClicked
       if(DataHolder.edit) {
        DataHolder.odpModeStatus = OdpEditEnum.EDIT.toString();
        HelperUtility.cleanOdpPlaylist();
        DataHolder.playingODP=false;
         DataHolder.ondemandCategories.setEnabled(true);
        DataHolder.isOdpEditLogoRequired = true;
        jLabel3.setEnabled(false);
        jLabel23.setEnabled(false);
        jLabel24.setEnabled(false);
        jLabel20.setEnabled(true);
        DataHolder.save=true;
        DataHolder.build=false;
        DataHolder.edit=false;
        DataHolder.delete=false;
        DataHolder.isPlaying = false;
        DataHolder.playerStatusLabel.setText("Player stopped");
        DataHolder.onDemandOverride = false;
        DataHolder.currentTrackLabel.setText("Stopped");
        DataHolder.nextTrackLabel.setText("Stopped");
        DataHolder.currenttrack = null;
        DataHolder.nextTrack = null;
        DataHolder.playListSelectedTrackPath = "";
        StreamPlayer.fadeOut();
        OnDemandPlayListPanelv2 panel = (OnDemandPlayListPanelv2) DataHolder.playList.getValueAt(
                DataHolder.playList.getSelectedRow(), DataHolder.playList.getSelectedColumn());
        for (Odpmedia.Playlist playlist : DataHolder.odpMedia.playlist) {
            if (playlist.id.equalsIgnoreCase(panel.getSongTitleLabel().getName())) {
                if (null == DataHolder.odpMedia) {
                    HelperUtility.initializeODPMedia();
                }
                DataHolder.odpPlaylistModel = (javax.swing.table.DefaultTableModel) DataHolder.odpPlayList.getModel();
                if (null == DataHolder.odpTrackList) {
                    DataHolder.odpTrackList = new LinkedList<Odpmedia.Playlist.Track>();
                } else {
                    DataHolder.odpTrackList.clear();
                }
                //todo place when the edit goes wrong for odp
                for (Odpmedia.Playlist.Track track : playlist.getTrack()) {
                    if (DataHolder.odpLastRow == DataHolder.odpPlayList.getRowCount()) {
                        DataHolder.odpPlaylistModel.addRow(new Object[]{});
                    }
                    DataHolder.odpTrackList.add(track);
                    HelperUtility.addOdpMaxtime(track.getTime());
                    DataHolder.odpPlaylistModel.setValueAt(track.getLabel(), DataHolder.odpLastRow, 0);
                    DataHolder.odpLastRow++;
                }
            }
        }
        HelperUtility.getOnDemandTrackListFromUpdatedMediaUpgraded(false);
        DataHolder.odpEditEnumStatus = OdpEditEnum.BUILD.toString();
       }// TODO add your handling code here:
    }//GEN-LAST:event_jLabel23MouseClicked
 public static boolean ondemandContainsTrack(OnDemandPlayListPanelv2 panel) {
        for (Media.Channels.Station.Programme.Playlist.Track track : DataHolder.onDemandTrackList) {
            if (track.getName().equalsIgnoreCase(panel.getSongTitleLabel().getName())) {
                if (!"".equalsIgnoreCase(track.getOndemandlabel())) {
                    if (track.getOndemandlabel().equalsIgnoreCase(panel.getSongTitleLabel().getText())) {
                        return true;
                    } else {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
    private void jTable1AncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jTable1AncestorMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable1AncestorMoved

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jLabel8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseClicked
try {
                    
                    jLabel8.setForeground(Color.GREEN);
                    
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                if (DataHolder.odpFilePath != null) {
                                    if (DataHolder.odpFile.exists()) {
                                        FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpPrimaryBackupFile);
                                        DataHolder.odpSecondaryBackupFile = new File(odpFileSecondaryBackupPath + FileSystems.getDefault().getSeparator()
                                                + DateUtil.getInstance().formatDate(new Date(), DateUtil.DATE_FORMAT_MMDDYYYY) + ".bk");
                                        if (!DataHolder.odpSecondaryBackupFile.getParentFile().exists()) {
                                            DataHolder.odpSecondaryBackupFile.getParentFile().mkdirs();
                                        }
                                        if (!DataHolder.odpSecondaryBackupFile.exists()) {
                                            DataHolder.odpSecondaryBackupFile.createNewFile();
                                        }
                                        FileUtils.copyFile(DataHolder.odpFile, DataHolder.odpSecondaryBackupFile);
                                        ExchangeODPService exchangeODPService = new ExchangeODPService();
                                        ODP odp = new ODP();
                                        odp.setPartnerName(DataHolder.configProperties.getProperty("userName"));
                                        odp.setMacID(DataHolder.macAddress);
                                        odp.setToken(DataHolder.configProperties.getProperty("DeviceToken"));
                                        BufferedReader bufferedReader = new BufferedReader(new FileReader(DataHolder.odpFile));
                                        StringBuilder stringBuilder = new StringBuilder();
                                        String line = null;
                                        while ((line = bufferedReader.readLine()) != null) {
                                            stringBuilder.append(line).append("\n");
                                        }
                                        odp.setBackUpText(stringBuilder.toString());
                                        exchangeODPService.getExchangeODPServiceSoap().setODPBackUp(odp);
                                        NeptuneLogger.getInstance().getLogger().log(Level.INFO, "SAVE --> ODPBackUp -----> SUCCESSFULL");
                                    }
                                }
                            } catch (Exception e) {
                                NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "ERROR IN setODPBackUp()", e);
                            } finally {
                                try {
                                    jLabel8.setForeground(Color.WHITE);
                                    //DataHolder.odpBackupLabel.setIcon(new ImageIcon(this.getClass().getResource("/images/odpbackup.png")));
                                } catch (Exception ex) {
                                    NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "odpFileWriteer Close Exception", ex);
                                }
                            }
                        }
                    }).start();
                } catch (Exception e) {
                    NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "odpFileWriteer Close Exception", e);
                }
                new Thread(new ScheduledTask()).start();
                // jLabel8.setForeground(Color.WHITE);        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel8MouseClicked

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
       try {
                   NeptuneLogger.getInstance().getLogger().info("-------------Refreshing--------------");
                  jLabel7.setForeground(Color.GREEN);
                  //sleep(5000);
                  //jLabel7.setForeground(Color.white);
                    new Thread(new Runnable() {
                        public void run() {
                            FileWriter odpFileWriteer = null;
                            try {

                                if (!DataHolder.odpFile.exists()) {
                                    DataHolder.odpFile.createNewFile();
                                }
                                if (DataHolder.odpPrimaryBackupFile.exists()) {
                                    FileUtils.copyFile(DataHolder.odpPrimaryBackupFile, DataHolder.odpFile);
                                } else {
                                    ExchangeODPService exchangeODPService = new ExchangeODPService();
                                    ODP odp = new ODP();
                                    odp.setPartnerName(DataHolder.configProperties.getProperty("userName"));
                                    odp.setMacID(DataHolder.macAddress);
                                    odp.setToken(DataHolder.configProperties.getProperty("DeviceToken"));
                                    ODP updatedOdp = exchangeODPService.getExchangeODPServiceSoap().getODPBackUp(odp);
                                    odpFileWriteer = new FileWriter(DataHolder.odpFile);
                                    if (null != updatedOdp.getBackUpText() && updatedOdp.getBackUpText().length() > 0) {
                                        odpFileWriteer.write(updatedOdp.getBackUpText());
                                    }
                                    NeptuneLogger.getInstance().getLogger().log(Level.INFO, "setODPBackUp() SUCCESSFULL");
                                }
                            } catch (Exception e) {
                                NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "ERROR IN setODPBackUp()", e);
                            } finally {
                                try {
                                      odpFileWriteer.close();
                                } catch (IOException ex) {
                                    NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "odpFileWriteer Close Exception", ex);
                                } catch(Exception e){
                                    try {
                                        sleep(1000);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(RadioMode.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                     jLabel7.setForeground(Color.white);
                                }
                            }
                        }
                    }).start();
                } catch (Exception e) {
                    NeptuneLogger.getInstance().getLogger().log(Level.SEVERE, "odpFileWriteer Close Exception", e);
                }
                new Thread(new ScheduledTask()).start();
                // TODO add your handling code here:
    }//GEN-LAST:event_jLabel7MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OnDemandMode.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OnDemandMode.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OnDemandMode.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OnDemandMode.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OnDemandMode().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    // End of variables declaration//GEN-END:variables


}

